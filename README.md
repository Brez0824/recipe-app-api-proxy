# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `App_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Post of the app to forward request to (default: `9000`)
